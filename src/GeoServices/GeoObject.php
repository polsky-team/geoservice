<?php

namespace GeoServices;

/**
 * Class GeoObject
 * @package GeoServices
 * @author Alexander S. <aleksandershtefan@gmail.com>
 */
class GeoObject
{

    public $ip;
    public $countryName;
    public $city;
    public $latitude;
    public $longitude;
    public $zip;
    public $regionName;
    public $countryCode; //2letter
    public $isp;
    public $method;

    public function __construct() {

    }

    /**
     * Возвращает массив свойств гео-объекта
     * @return array
     */
    public static function getProperties() {
        return array('countryName', 'city', 'latitude', 'longitude', 'zip', 'regionName', 'countryCode', 'isp');
    }

}
